module.exports = {
  collectCoverageFrom: ['src/**/*.js'],
  coverageReporters: ['text', 'html'],
};
